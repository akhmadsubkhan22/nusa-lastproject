import React from 'react'
import Link from 'next/link'
import { Breadcrumb as AntdBreadcrumb } from 'antd'
import { HomeOutlined, RightOutlined } from '@ant-design/icons'

function BreadCrumb({ items }) {
  const currentPath =
    typeof window !== 'undefined' ? window.location.pathname : ''

  const isAboutUsPath = currentPath === '/aboutus'

  return (
    <div className="mb-[25.5px] z-[100]">
      <AntdBreadcrumb separator={<RightOutlined />}>
        <AntdBreadcrumb.Item className="cursor-pointer">
          <Link href="/" passHref className="cursor-pointer">
            <HomeOutlined
              style={{
                fontSize: '18px',
                cursor: 'pointer',
                color: isAboutUsPath ? 'white' : 'initial',
              }}
            />
          </Link>
        </AntdBreadcrumb.Item>
        {items.map((item, index) => (
          <AntdBreadcrumb.Item key={index}>
            {item.link ? (
              <Link href={item.link} passHref>
                <p
                  className={`text-${
                    isAboutUsPath
                      ? 'bg-gray-900 text-white hover:underline'
                      : 'gray-500'
                  } text-[18px]`}
                >
                  {item.text}
                </p>
              </Link>
            ) : (
              <span
                className={`text-${
                  isAboutUsPath ? 'bg-gray-900 text-white' : 'gray-500'
                } text-[18px]`}
              >
                {item.text}
              </span>
            )}
          </AntdBreadcrumb.Item>
        ))}
      </AntdBreadcrumb>
    </div>
  )
}

export default BreadCrumb
